# create volume to mount data between host and docker ftp_server

sudo docker volume create --name ftp_data

sudo docker build -t ftp_server .
## USERS examples
# user|password

sudo docker run \
            -v ftp_data:/ftp \
            -p 25:21 \
            -p 21000-21010:21000-21010 \
            -e USERS="admin|ftpserver" \
            ftp_server